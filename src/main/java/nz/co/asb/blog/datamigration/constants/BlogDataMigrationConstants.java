package nz.co.asb.blog.datamigration.constants;

import java.util.HashMap;
import java.util.Map;


public class BlogDataMigrationConstants {
	public static final String TAG_ITEM = "item";
	public static final String TAG_TITLE = "title";
	public static final String TAG_DESCRIPTION = "description";
	public static final String TAG_CATEGORY = "category";
	public static final String TAG_CATEGORY_DOMAIN = "domain";
	public static final String TAG_CATEGORY_NICENAME = "nicename";
	public static final String TAG_PUBLISH_DATE = "pubDate";
	public static final String TAG_LINK = "link";
	public static final String TAG_POST_ID = "wp:post_id";
	public static final String TAG_POST_PARENT = "wp:post_parent";
	public static final String TAG_CONTENT_ENCODED = "content:encoded";
	public static final String TAG_ATTACHMENT_URL = "wp:attachment_url";
	public static final String TAG_WP_TAG = "wp:tag";
	public static final String TAG_WP_TAG_SLUG = "wp:tag_slug";
	public static final String TAG_WP_TAG_NAME = "wp:tag_name";
    public static final String TAG_WP_POST_NAME = "wp:post_name";
    public static final String TAG_WP_COMMENT = "wp:comment";
    public static final String TAG_WP_COMMENT_ID = "wp:comment_id";
    public static final String TAG_WP_COMMENT_PARENT_ID = "wp:comment_parent";
    public static final String TAG_WP_COMMENT_TIME = "wp:comment_date_gmt";
    public static final String TAG_WP_COMMENT_AUTHOR_IP = "wp:comment_author_IP";
    public static final String TAG_WP_COMMENT_AUTHOR_EMAIL = "wp:comment_author_email";
    public static final String TAG_WP_COMMENT_COMMENT_CONTENT = "wp:comment_content";
    public static final String TAG_WP_COMMENT_APPROVED = "wp:comment_approved";
    public static final String TAG_WP_COMMENT_USER_ID = "wp:comment_author";
    public static final String TAG_WP_COMMENT_AUTHOR_URL = "wp:comment_author_url";
    public static final String TAG_WP_META_VALUE = "wp:meta_value";
	
	public static final String PREFIX_CONTENT_PATH = "/content/ASB-Blog/en/posts/";
	public static final String PREFIX_DAM_IMAGE = "/content/dam/ASB/";
	public static final String PREFIX_TAG = "/etc/tags/ASB/";
	public static final String PREFIX_COMMENTS = "/content/usergenerated/content/ASB-Blog";
	public static final String JCR_CONTENT = "jcr:content";
	public static final String PAR = "par";
	public static final String ALT = "alt";
	public static final String BANNERIMAGE = "bannerimage";
    public static final String CAPTIONEDIMAGE = "captionedimage";
	public static final String ENTRY = "entry";
	
	public static final String NODE_TYPE_PAGE = "cq:Page";
	public static final String NODE_TYPE_PAGE_CONTENT = "cq:PageContent";
	public static final String NODE_TYPE_UNSTRUCTURED = "nt:unstructured";
	
	public static final String PROPERTY_CQ_TEMPLATE = "cq:template";
	public static final String PROPERTY_TITLE = "jcr:title";
	public static final String PROPERTY_DESCRIPTION = "description";
    public static final String PROPERTY_JCR_DESCRIPTION = "jcr:description";
	public static final String PROPERTY_RESOURCE_TYPE = "sling:resourceType";
    public static final String PROPERTY_ALIGNMENT = "alignment";

	public static final String PROPERTY_TAGS = "cq:tags";
	public static final String PROPERTY_CATEGORIES = "categories";
	public static final String PROPERTY_PUBLISH_DATE = "publishdate";
	public static final String PROPERTY_LIKE = "link";
	public static final String PROPERTY_FILE_REFERENCE = "fileReference";
	public static final String PROPERTY_REFERENCE = "reference";
	public static final String PROPERTY_REQUIRE_LOGIN = "requireLogin";
	public static final String PROPERTY_ALLOW_REPLIES_TO_COMMENTS = "allowRepliesToComments";
	public static final String PROPERTY_DISPLAY_COMMENTS_AS_TREE = "displayCommentsAsTree";
	public static final String PROPERTY_TEXT_IS_RICH = "textIsRich";
	public static final String PROPERTY_WIDTH = "width";
	public static final String PROPERTY_HEIGHT = "height";
	
	public static final String AUTHOR_TEMPLATE = "/apps/asb/templates/blog/blog-author-template";
	public static final String AUTHOR_PAGE = "asb/components/page/blog/author-page";
	public static final String PARSYS = "foundation/components/parsys";
	public static final String RESOURCE_BANNERIMAGE = "asb/components/blog/bannerimage";
    public static final String RESOURCE_CAPTIONEDIMAGE = "asb/components/blog/captionedimage";
	public static final String TEXT = "foundation/components/text";
	public static final String IMAGE = "foundation/components/image";
	public static final String YOUTUBE = "asb/components/blog/youtube";
	public static final String TWITTER = "asb/components/blog/twitter";
	public static final String COMMENT = "social/commons/components/comments";

	public static final String CATEGORY_YOUR_MONEY = "your-money";
	public static final String CATEGORY_YOUR_COMUNITY = "your-community";
	public static final String CATEGORY_TECH_INNOVATION = "tech-innovation";
	public static final String CATEGORY_NEWS_STORIES = "news-stories";
	
	public static final String VALUE_POST_PARENT_ZERO = "0";
	public static final String VALUE_COMMENT_PARENT_ZERO = "0";
	public static final String VALUE_COMMENT_APPROVED_TRUE = "1";
	
	public static final String DOMAIN_POST_TAG = "post_tag";
	public static final String DOMAIN_CATEGORY = "category";
	
	public static final Map<String, Integer> MONTHS = new HashMap<String, Integer>();
	
	static {
		MONTHS.put("jan", 0);
		MONTHS.put("feb", 1);
		MONTHS.put("mar", 2);
		MONTHS.put("apr", 3);
		MONTHS.put("may", 4);
		MONTHS.put("jun", 5);
		MONTHS.put("jul", 6);
		MONTHS.put("aug", 7);
		MONTHS.put("sep", 8);
		MONTHS.put("oct", 9);
		MONTHS.put("nov", 10);
		MONTHS.put("dec", 11);
	}
}
