package nz.co.asb.blog.datamigration.servlet;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;

import nz.co.asb.blog.datamigration.service.BlogDataDownloadService;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;

@Component(metatype = true, label = "Blog Data Download Servlet", description = "Servlet to download blog data from xml file to crx")
@Service
@Property(name = "sling.servlet.paths", value = "/bin/dataDownload", propertyPrivate = true)
public class BlogDataDownloadServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 937273557678133578L;
	@Reference
	private BlogDataDownloadService blogDataDownloadService;
	
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}
	
	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		String fileLocation = request.getParameter("file");
		
		InputStream fileStream = null;
		
		try {
			fileStream =  new FileInputStream(fileLocation);
			blogDataDownloadService.downloadBlogData(fileStream);
			response.getWriter().write("<h3>Data download is complete.</h3>");
		}
		catch(Exception e) {
			response.getWriter().write("<h3>Exception occurred.</h3>");
			response.getWriter().write(e.toString());
		}
		finally {
			if(fileStream != null) {
				fileStream.close();
			}
		}	
	}
}
