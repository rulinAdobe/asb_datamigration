package nz.co.asb.blog.datamigration.service;

import java.io.InputStream;

public interface BlogDataMigrationService {
	void migrateBlogData(InputStream xmlInputStream);
}
