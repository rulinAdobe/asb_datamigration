package nz.co.asb.blog.datamigration.service.impl;

import com.adobe.cq.social.commons.CollabUser;
import com.adobe.cq.social.commons.Comment;
import com.adobe.cq.social.commons.CommentSystem;
import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.tagging.InvalidTagFormatException;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import nz.co.asb.blog.datamigration.constants.BlogDataMigrationConstants;
import nz.co.asb.blog.datamigration.service.BlogDataMigrationService;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.jcr.*;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.nodetype.NoSuchNodeTypeException;
import javax.jcr.version.VersionException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.InputStream;
import java.security.AccessControlException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component(immediate = true, label = "Migrate Blog Data Service", description = "Migrate blog data from XML file into CRX")
@Service
public class BlogDataMigrationServiceImpl implements BlogDataMigrationService {

    private Logger logger = LoggerFactory.getLogger(BlogDataMigrationServiceImpl.class);
    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    public void migrateBlogData(InputStream xmlInputStream) {
        SAXParserFactory parserFactory = SAXParserFactory.newInstance();
        DefaultHandler handler = null;

        try {
            SAXParser parser = parserFactory.newSAXParser();
            handler = new ParseHandler(resourceResolverFactory);
            parser.parse(xmlInputStream, handler);
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    class ParseHandler extends DefaultHandler {
        private Logger logger = LoggerFactory.getLogger(ParseHandler.class);
        private ResourceResolverFactory resourceResolverFactory = null;
        private ResourceResolver adminResourceResolver = null;
        private TagManager tagMgr = null;
        private Session session = null;
        private Map<String, XmlItem> blogEntriesMap = new HashMap<String, XmlItem>();
        private Map<String, XmlItem> attachmentsMap = new HashMap<String, XmlItem>();
        private List<TagItem> tagList = new LinkedList<TagItem>();
        private XmlItem currentBlogItem = null;
        private TagItem currentTagItem = null;
        private StringBuffer currentValue = null;
        private Attributes currentAttributes = null;
        private CommentItem currentCommentItem = null;

        ParseHandler(ResourceResolverFactory resourceResolverFactory) {
            this.resourceResolverFactory = resourceResolverFactory;
        }

        public void startDocument() throws SAXException {

        }

        public void endDocument() throws SAXException {
            if (blogEntriesMap != null && blogEntriesMap.size() > 0) {
                try {
                    adminResourceResolver = resourceResolverFactory.getAdministrativeResourceResolver(null);
                    session = adminResourceResolver.adaptTo(Session.class);
                    tagMgr = adminResourceResolver.adaptTo(TagManager.class);

                    createTags();
                    createBlogNodes();
                    session.save();

                } catch (LoginException e) {
                    logger.error(e.toString(), e);
                } catch (RepositoryException e) {
                    logger.error(e.toString(), e);
                } catch (AccessControlException e) {
                    logger.error(e.toString(), e);
                } catch (InvalidTagFormatException e) {
                    logger.error(e.toString(), e);
                } finally {
                    if (adminResourceResolver != null) {
                        adminResourceResolver.close();
                    }
                }
            }
        }

        private void createTags() throws AccessControlException, InvalidTagFormatException {
            for (TagItem item : tagList) {
                String tagID = BlogDataMigrationConstants.PREFIX_TAG + item.tagSlug;
                String title = item.tagName;
                String description = item.tagName;

                tagMgr.createTag(tagID, title, description, false);
            }
        }

        private void createBlogNodes() throws RepositoryException, AccessControlException, InvalidTagFormatException {
            cleanExistingPageNodes();
            cleanExistingCommentNodes();

            for (XmlItem item : blogEntriesMap.values()) {
                Node node = getBlogEntryNode(item);
                Node jcrContentNode = setJcrContentProperties(node, item);
                createComment(item, jcrContentNode);
            }
        }

        private void cleanExistingPageNodes() throws RepositoryException {
            if (session.nodeExists(BlogDataMigrationConstants.PREFIX_CONTENT_PATH)) {
                Node parentNode = session.getNode(BlogDataMigrationConstants.PREFIX_CONTENT_PATH);
                NodeIterator iterator = parentNode.getNodes();
                List<Node> children = new LinkedList<Node>();

                while (iterator.hasNext()) {
                    Node node = iterator.nextNode();
                    if (!node.getName().equalsIgnoreCase(BlogDataMigrationConstants.JCR_CONTENT)) {
                        children.add(node);
                    }
                }

                for (Node node : children) {
                    node.remove();
                }
            }
        }

        private void cleanExistingCommentNodes() throws PathNotFoundException, RepositoryException {
            if (session.nodeExists(BlogDataMigrationConstants.PREFIX_COMMENTS)) {
                Node node = session.getNode(BlogDataMigrationConstants.PREFIX_COMMENTS);
                node.remove();
            }
        }

        private void createComment(XmlItem item, Node jcrContentNode) throws RepositoryException {
            Map<String, List<CommentItem>> commentsMap = item.commentsMap;
            String commentBasedPath = jcrContentNode.getPath() + "/alt";
            createComment(commentsMap, BlogDataMigrationConstants.VALUE_COMMENT_PARENT_ZERO, commentBasedPath);
        }

        private void createComment(Map<String, List<CommentItem>> commentsMap, String parentId, String commentBasedPath) {
            List<CommentItem> commentsList = commentsMap.get(parentId);

            if (commentsList != null && commentsList.size() > 0) {

                for (CommentItem commentItem : commentsList) {
                    Resource res = adminResourceResolver.resolve(commentBasedPath);
                    CommentSystem cs = res.adaptTo(CommentSystem.class);
                    Comment comment = cs.addComment(commentItem.conmentDescription, commentItem.userId);
                    setCommentProperties(commentItem, comment);
                    cs.save();

                    createComment(commentsMap, commentItem.id, comment.getPath());
                }
            }
        }

        private void setCommentProperties(CommentItem commentItem, Comment comment) {
            final ModifiableValueMap properties = comment.getResource().adaptTo(ModifiableValueMap.class);

            //IP
            properties.put(Comment.PROP_IP_ADDRESS, commentItem.authorIP);
            //Time
            String[] timeArray = commentItem.commentTime.split(" ");
            String[] date = timeArray[0].split("-");
            String[] time = timeArray[1].split(":");

            Calendar cal = Calendar.getInstance();
            cal.set(Integer.parseInt(date[0]), Integer.parseInt(date[1]) - 1, Integer.parseInt(date[2]), Integer.parseInt(time[0]), Integer.parseInt(time[1]), Integer.parseInt(time[2]));

            properties.put(Comment.PROP_DATE, cal);
            //email
            properties.put(CollabUser.PROP_EMAIL, commentItem.authorEmail);
            //Sentiment
            properties.put("positive", new Long(0));
            properties.put("negative", new Long(0));
            //Approval
            properties.put(Comment.PROP_APPROVED, new Boolean(true));
            //user id
            properties.put("userIdentifier", commentItem.userId);
            properties.put("authorizableId", commentItem.userId);
            //others
            properties.put(Comment.PROP_USER_AGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36");
            //properties.put("jcr:mixinTypes", new Name[1]{})
            properties.put("sentiment", new Long(0));

            //author url
            properties.put("url", commentItem.authorUrl);
        }

        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (BlogDataMigrationConstants.TAG_ITEM.equalsIgnoreCase(qName)) {
                currentBlogItem = new XmlItem();
            } else if (BlogDataMigrationConstants.TAG_WP_TAG.equalsIgnoreCase(qName)) {
                currentTagItem = new TagItem();
            } else if (BlogDataMigrationConstants.TAG_WP_COMMENT.equalsIgnoreCase(qName)) {
                currentCommentItem = new CommentItem();
            }

            currentAttributes = attributes;
        }

        public void endElement(String uri, String localName, String qName) throws SAXException {
            if (BlogDataMigrationConstants.TAG_ITEM.equalsIgnoreCase(qName)) {
                endItemTag();
            } else if (BlogDataMigrationConstants.TAG_WP_TAG.equalsIgnoreCase(qName)) {
                endWpTagTag();
            } else if (BlogDataMigrationConstants.TAG_WP_COMMENT.equalsIgnoreCase(qName)) {
                endWpCommentTag();
            } else if (BlogDataMigrationConstants.TAG_TITLE.equalsIgnoreCase(qName)) {
                if (currentBlogItem != null) {
                    currentBlogItem.title = currentValue.toString();
                }
            } else if (BlogDataMigrationConstants.TAG_DESCRIPTION.equalsIgnoreCase(qName)) {
                if (currentBlogItem != null) {
                    currentBlogItem.description = currentValue.toString();
                }
            } else if (BlogDataMigrationConstants.TAG_PUBLISH_DATE.equalsIgnoreCase(qName)) {
                if (currentBlogItem != null) {
                    setPublishDate();
                }
            } else if (BlogDataMigrationConstants.TAG_LINK.equalsIgnoreCase(qName)) {
                if (currentBlogItem != null) {
                    currentBlogItem.link = currentValue.toString();
                }
            } else if (BlogDataMigrationConstants.TAG_CATEGORY.equalsIgnoreCase(qName)) {
                if (currentBlogItem != null && currentAttributes != null) {
                    setCategoryOrTag();
                }
            } else if (BlogDataMigrationConstants.TAG_POST_ID.equalsIgnoreCase(qName)) {
                if (currentBlogItem != null) {
                    currentBlogItem.postId = currentValue.toString();
                }
            } else if (BlogDataMigrationConstants.TAG_POST_PARENT.equalsIgnoreCase(qName)) {
                if (currentBlogItem != null) {
                    currentBlogItem.postParent = currentValue.toString();
                }
            } else if (BlogDataMigrationConstants.TAG_CONTENT_ENCODED.equalsIgnoreCase(qName)) {
                if (currentBlogItem != null) {
                    currentBlogItem.content = currentValue.toString();
                }
            } else if (BlogDataMigrationConstants.TAG_WP_POST_NAME.equalsIgnoreCase(qName)) {
                if (currentBlogItem != null && currentValue != null && currentValue.toString().trim().length() > 0) {
                    currentBlogItem.nodeName = currentValue.toString();
                }
            } else if (BlogDataMigrationConstants.TAG_WP_TAG_SLUG.equalsIgnoreCase(qName)) {
                if (currentValue != null) {
                    currentTagItem.tagSlug = currentValue.toString();
                }
            } else if (BlogDataMigrationConstants.TAG_WP_TAG_NAME.equalsIgnoreCase(qName)) {
                if (currentValue != null) {
                    currentTagItem.tagName = currentValue.toString();
                }
            } else if (BlogDataMigrationConstants.TAG_WP_COMMENT_ID.equalsIgnoreCase(qName)) {
                if (currentCommentItem != null) {
                    currentCommentItem.id = currentValue.toString();
                }
            } else if (BlogDataMigrationConstants.TAG_WP_COMMENT_PARENT_ID.equalsIgnoreCase(qName)) {
                if (currentCommentItem != null) {
                    currentCommentItem.parentId = currentValue.toString();
                }
            } else if (BlogDataMigrationConstants.TAG_WP_COMMENT_TIME.equalsIgnoreCase(qName)) {
                if (currentCommentItem != null) {
                    currentCommentItem.commentTime = currentValue.toString();
                }
            } else if (BlogDataMigrationConstants.TAG_WP_COMMENT_AUTHOR_IP.equalsIgnoreCase(qName)) {
                if (currentCommentItem != null) {
                    currentCommentItem.authorIP = currentValue.toString();
                }
            } else if (BlogDataMigrationConstants.TAG_WP_COMMENT_AUTHOR_EMAIL.equalsIgnoreCase(qName)) {
                if (currentCommentItem != null) {
                    currentCommentItem.authorEmail = currentValue.toString();
                }
            } else if (BlogDataMigrationConstants.TAG_WP_COMMENT_COMMENT_CONTENT.equalsIgnoreCase(qName)) {
                if (currentCommentItem != null) {
                    currentCommentItem.conmentDescription = currentValue.toString();
                }
            } else if (BlogDataMigrationConstants.TAG_WP_COMMENT_USER_ID.equalsIgnoreCase(qName)) {
                if (currentCommentItem != null) {
                    currentCommentItem.userId = currentValue.toString();
                }
            } else if (BlogDataMigrationConstants.TAG_WP_COMMENT_AUTHOR_URL.equalsIgnoreCase(qName)) {
                if (currentCommentItem != null) {
                    currentCommentItem.authorUrl = currentValue.toString();
                }
            } else if (BlogDataMigrationConstants.TAG_WP_META_VALUE.equalsIgnoreCase(qName)) {
                if (currentBlogItem != null && currentValue != null) {
                    String value = currentValue.toString();
                    if (value.matches(".*src=\"(https|http)://www.youtube.com/.*")) {
                        String link = Jsoup.parse(value).select("iframe").attr("src");

                        if (link.contains("http://")) {
                            link = link.replace("http://", "https://");
                        }

                        if (!currentBlogItem.youtubeLinkList.contains(link))
                            currentBlogItem.youtubeLinkList.add(link);
                    }
                }
            }

            currentAttributes = null;
            currentValue = null;
        }

        private void endItemTag() {
            if (BlogDataMigrationConstants.VALUE_POST_PARENT_ZERO.equals(currentBlogItem.postParent)) {
                blogEntriesMap.put(currentBlogItem.postId, currentBlogItem);
            } else {
                attachmentsMap.put(currentBlogItem.postId, currentBlogItem);
            }

            currentBlogItem = null;
        }

        private void endWpTagTag() {
            if (currentTagItem != null) {
                tagList.add(currentTagItem);
            }

            currentTagItem = null;
        }

        private void endWpCommentTag() {
            if (currentCommentItem != null && currentBlogItem != null) {
                String parentId = currentCommentItem.parentId;

                if (parentId != null) {
                    List<CommentItem> list = currentBlogItem.commentsMap.get(parentId);

                    if (list == null) {
                        list = new LinkedList<CommentItem>();
                    }

                    list.add(currentCommentItem);

                    currentBlogItem.commentsMap.put(parentId, list);
                }
            }

            currentCommentItem = null;
        }

        public void characters(char[] ch, int start, int length) {
            if (currentValue == null) {
                currentValue = new StringBuffer();
            }

            String value = new String(ch, start, length).trim();
            currentValue.append(value);
        }

        private void setCategoryOrTag() {
            String domain = null;
            String nicename = null;

            for (int i = 0; i < currentAttributes.getLength(); i++) {
                if (BlogDataMigrationConstants.TAG_CATEGORY_DOMAIN.equalsIgnoreCase(currentAttributes.getQName(i))) {
                    domain = currentAttributes.getValue(i);
                } else if (BlogDataMigrationConstants.TAG_CATEGORY_NICENAME.equalsIgnoreCase(currentAttributes.getQName(i))) {
                    nicename = currentAttributes.getValue(i);
                }
            }

            if (BlogDataMigrationConstants.DOMAIN_POST_TAG.equalsIgnoreCase(domain)) {
                currentBlogItem.tags.add(nicename);
            } else if (BlogDataMigrationConstants.DOMAIN_CATEGORY.equalsIgnoreCase(domain)) {
                if (!nicename.equalsIgnoreCase("slides"))
                    currentBlogItem.categories.add(nicename);
            }
        }

        private void setPublishDate() {
            String[] array = currentValue.toString().split("\\s");
            String[] time = array[4].split(":");

            Calendar c = Calendar.getInstance();
            c.set(Integer.parseInt(array[3]),
                    BlogDataMigrationConstants.MONTHS.get(array[2].toLowerCase()),
                    Integer.parseInt(array[1]),
                    Integer.parseInt(time[0]),
                    Integer.parseInt(time[1]),
                    Integer.parseInt(time[2]));

            // get Newzeland time
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//            sdf.setTimeZone(TimeZone.getTimeZone("GMT+12"));
//            String[] newzelandDate = sdf.format(c.getTime()).split("-");

            currentBlogItem.publishDate = c.getTime();
        }

        private Node getBlogEntryNode(XmlItem item) throws RepositoryException {
            String nodeName = item.nodeName;
            //String[] array = item.publishDate.split("/");
            Calendar cal = Calendar.getInstance();
            cal.setTime(item.publishDate);
            String year = String.valueOf(cal.get(Calendar.YEAR));

            int monthVal = cal.get(Calendar.MONTH) + 1;
            String month = String.valueOf((monthVal > 9) ? monthVal : "0" + monthVal);
            String yearFolderPath = BlogDataMigrationConstants.PREFIX_CONTENT_PATH + year;
            String monthFolderPath = yearFolderPath + "/" + month;
            String nodePath = monthFolderPath + "/" + nodeName;
            Node node = null;
            Node contentNode = null;

            if (!session.nodeExists(yearFolderPath)) {
                node = JcrUtil.createPath(yearFolderPath, BlogDataMigrationConstants.NODE_TYPE_PAGE, session);
                contentNode = node.addNode(BlogDataMigrationConstants.JCR_CONTENT, BlogDataMigrationConstants.NODE_TYPE_UNSTRUCTURED);

                contentNode.setProperty("blogarchive", year);
                contentNode.setProperty(BlogDataMigrationConstants.PROPERTY_TITLE, year);
                contentNode.setProperty(BlogDataMigrationConstants.PROPERTY_RESOURCE_TYPE, BlogDataMigrationConstants.AUTHOR_PAGE);
            }

            if (!session.nodeExists(monthFolderPath)) {
                node = JcrUtil.createPath(monthFolderPath, BlogDataMigrationConstants.NODE_TYPE_PAGE, session);
                contentNode = node.addNode(BlogDataMigrationConstants.JCR_CONTENT, BlogDataMigrationConstants.NODE_TYPE_UNSTRUCTURED);

                contentNode.setProperty("blogarchive", year + "/" + month);
                contentNode.setProperty(BlogDataMigrationConstants.PROPERTY_TITLE, month);
                contentNode.setProperty(BlogDataMigrationConstants.PROPERTY_RESOURCE_TYPE, BlogDataMigrationConstants.AUTHOR_PAGE);
            }

            node = JcrUtil.createPath(nodePath, BlogDataMigrationConstants.NODE_TYPE_PAGE, session);
            return node;
        }

        private Node setJcrContentProperties(Node node, XmlItem item) throws PathNotFoundException, ItemExistsException, VersionException, ConstraintViolationException, LockException, RepositoryException, AccessControlException, InvalidTagFormatException {
            Node contentNode = null;
            if (node.hasNode(BlogDataMigrationConstants.JCR_CONTENT)) {
                contentNode = node.getNode(BlogDataMigrationConstants.JCR_CONTENT);
            } else {
                contentNode = node.addNode(BlogDataMigrationConstants.JCR_CONTENT, BlogDataMigrationConstants.NODE_TYPE_PAGE_CONTENT);
            }

            contentNode.setProperty(BlogDataMigrationConstants.PROPERTY_CQ_TEMPLATE, BlogDataMigrationConstants.AUTHOR_TEMPLATE);
            contentNode.setProperty(BlogDataMigrationConstants.PROPERTY_RESOURCE_TYPE, BlogDataMigrationConstants.AUTHOR_PAGE);

            if (item.title != null) {
                contentNode.setProperty(BlogDataMigrationConstants.PROPERTY_TITLE, item.title);
            }
            if (item.description != null) {
                contentNode.setProperty(BlogDataMigrationConstants.PROPERTY_DESCRIPTION, item.description);
            }
            if (item.publishDate != null) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(item.publishDate);
                contentNode.setProperty(BlogDataMigrationConstants.PROPERTY_PUBLISH_DATE, cal);
            }
            if (item.link != null) {
                contentNode.setProperty(BlogDataMigrationConstants.PROPERTY_LIKE, item.link);
            }
            if (item.categories.size() > 0) {
                contentNode.setProperty(BlogDataMigrationConstants.PROPERTY_CATEGORIES, item.categories.toArray(new String[0]));
            }
            if (item.tags.size() > 0) {
                List<Tag> tags = new LinkedList<Tag>();
                for (String name : item.tags) {
                    String tagID = BlogDataMigrationConstants.PREFIX_TAG + name;
                    Tag tag = tagMgr.resolve(tagID);
                    if (tag != null)
                        tags.add(tag);
                }

                Resource resource = this.adminResourceResolver.getResource(contentNode.getPath());
                tagMgr.setTags(resource, tags.toArray(new Tag[0]), false);
            }

            setContentProperties(contentNode, item);

            // create the alt node used for social comment component 
            createAltNode(contentNode);

            return contentNode;
        }

        private String getValue(String value, String key) {
            if (value.startsWith(key)) {
                Pattern p = Pattern.compile("\"([a-z0-9A-Z./:-]*)\"*");
                Matcher m = p.matcher(value);
                m.find();
                return m.group(1);
            }
            return "";
        }

        private void createAltNode(Node contentNode) throws RepositoryException {
            Node altNode = null;
            if (contentNode.hasNode(BlogDataMigrationConstants.ALT)) {
                altNode = contentNode.getNode(BlogDataMigrationConstants.ALT);
            } else {
                altNode = contentNode.addNode(BlogDataMigrationConstants.ALT, BlogDataMigrationConstants.NODE_TYPE_UNSTRUCTURED);
            }

            altNode.setProperty(BlogDataMigrationConstants.PROPERTY_RESOURCE_TYPE, BlogDataMigrationConstants.COMMENT);
            altNode.setProperty(BlogDataMigrationConstants.PROPERTY_REQUIRE_LOGIN, "false");
            altNode.setProperty(BlogDataMigrationConstants.PROPERTY_ALLOW_REPLIES_TO_COMMENTS, new Boolean(true));
            altNode.setProperty(BlogDataMigrationConstants.PROPERTY_DISPLAY_COMMENTS_AS_TREE, new Boolean(true));
        }

        private void setContentProperties(Node node, XmlItem item) throws RepositoryException {

            Node parNode = null;

            if (item.content != null && item.content.length() > 0) {
                parNode = createParNode(node);

                Scanner contentScanner = new Scanner(item.content);
                int lineNum = 0;
                int textNum = 0;
                int imageNum = 0;
                int youtubeNum = 0;
                int twitterNum = 0;
                boolean isInBlock = false;
                StringBuilder currentBlock = null;
                String terminateTag = null;
                try {
                    while (contentScanner.hasNextLine()) {
                        String paragraph = contentScanner.nextLine().trim().replaceAll("&nbsp;", "");
                        logger.debug("PAragraph::" + paragraph);
                        if (paragraph.length() <= 0)
                            continue;

                        if (isInBlock) {
                            currentBlock.append(paragraph);
                            if (paragraph.contains(terminateTag)) {
                                boolean isNodeCreated = createTextNode(parNode, currentBlock.toString(), textNum);
                                if (isNodeCreated) {
                                    textNum++;
                                }
                                isInBlock = false;
                                currentBlock = null;
                                terminateTag = null;
                            }
                        } else {
                            if (paragraph.contains("[caption id=")) {
                                String[] captionParaItems = paragraph.split(" ");
                                String align = null, alt = null, width = null, src = null, height = null, desc = null;
                                for (String text : captionParaItems) {
                                    if (text.startsWith("align")) {
                                        align = getValue(text, "align");
                                    }
                                    if (text.startsWith("alt")) {
                                        alt = getValue(text, "alt");
                                    }
                                    if (text.startsWith("width")) {
                                        width = getValue(text, "width");
                                    }
                                    if (text.startsWith("height")) {
                                        width = getValue(text, "height");
                                    }
                                    if (text.startsWith("src")) {
                                        src = getValue(text, "src");
                                    }
                                }

                                Pattern p = Pattern.compile("([a-zA-z0-9 ]*)\\[/caption]");
                                Matcher m = p.matcher(paragraph);
                                m.find();
                                desc = m.group(1);

                                logger.debug("\n\n\n=========================================");
                                logger.debug("sling:resourceType:::" + "asb/components/blog/captionedimage");
                                logger.debug("imageRotate:::" + "imageRotate=0");
                                logger.debug("src:::" + src);
                                logger.debug("align:::" + align);
                                logger.debug("alt:::" + alt);
                                logger.debug("width:::" + width);
                                logger.debug("src:::" + src);
                                logger.debug("=========================================");
                                createCaptionImageNode(parNode, align, src, desc, alt, width, height);
                                continue;

                            }
                            if (paragraph.matches(".*<img.*/>.*")) {
                                Document doc = Jsoup.parse(paragraph);

                                for (org.jsoup.nodes.Node childNode : doc.body().childNodes()) {
                                    if (childNode instanceof Element && ((Element) childNode).select("img").size() > 0) {
                                        for (Element element : ((Element) childNode).select("img")) {
                                            String reference = element.attr("src");
                                            reference = BlogDataMigrationConstants.PREFIX_DAM_IMAGE + reference.substring(reference.indexOf("upload"));

                                            String width = element.attr("width");
                                            String height = element.attr("height");

                                            if (lineNum == 0) {
                                                createBannerImageNode(parNode, reference, width, height);
                                            } else {
                                                boolean isNodeCreated = createImageNode(parNode, reference, width, height, imageNum);
                                                if (isNodeCreated) {
                                                    imageNum++;
                                                }
                                            }
                                        }
                                    } else {
                                        String childNodeText = childNode.toString();
                                        boolean isNodeCreated = createTextNode(parNode, childNodeText, textNum);
                                        if (isNodeCreated) {
                                            textNum++;
                                        }
                                    }
                                }
                            } else if (paragraph.matches(".*[^(href=\")][(http)|(https)]://www.youtube.com/.*") || paragraph.matches(".*[^(href=\")][(http)|(https)]://youtu.be/.*")) {
                                String link = null;

                                if (paragraph.contains("<iframe")) {
                                    link = getReferenceLink(paragraph);
                                } else if (!item.youtubeLinkList.isEmpty()) {
                                    link = item.youtubeLinkList.get(0);
                                    item.youtubeLinkList.remove(0);
                                }

                                if (link != null) {
                                    boolean isNodeCreated = createYouTubeNode(parNode, link, youtubeNum);
                                    if (isNodeCreated) {
                                        youtubeNum++;
                                    }
                                }
                            } else if (paragraph.matches("(http|https)://twitter.com/.*")) {
                                String link = getReferenceLink(paragraph);
                                boolean isNodeCreated = createTwitterNode(parNode, link, twitterNum);
                                if (isNodeCreated) {
                                    twitterNum++;
                                }
                            } else if (paragraph.matches("^http://instagram.com/p/dSujc0jCK2/.*")) {
                                // some special case for images on instagram
                                String reference = BlogDataMigrationConstants.PREFIX_DAM_IMAGE + "distilleryimage6.ak.instagram.com/171c65fa0ab411e38e2522000aeb0d71_6.jpg";

                                boolean isNodeCreated = createImageNode(parNode, reference, "306", "306", imageNum);
                                if (isNodeCreated) {
                                    imageNum++;
                                }
                            } else {
                                if (paragraph.contains("<table") && !paragraph.contains("</table>")) {
                                    isInBlock = true;
                                    currentBlock = new StringBuilder(paragraph);
                                    terminateTag = "</table>";
                                } else if (paragraph.contains("<ul") && !paragraph.contains("</ul>")) {
                                    isInBlock = true;
                                    currentBlock = new StringBuilder(paragraph);
                                    terminateTag = "</ul>";
                                } else {
                                    boolean isNodeCreated = createTextNode(parNode, paragraph, textNum);
                                    if (isNodeCreated) {
                                        textNum++;
                                    }
                                }
                            }

                            lineNum++;
                        }
                    }
                } finally {
                    contentScanner.close();
                }
            }

        }

        private String getReferenceLink(String paragraph) {
            int i = paragraph.indexOf("http");
            int j = i;
            char[] array = paragraph.toCharArray();
            for (; j < array.length; j++) {
                if ((array[j] == ' ') || (array[j] == '\"') || (array[j] == '[') || (array[j] == ']'))
                    break;
            }

            return paragraph.substring(i, j);
        }

        private Node createParNode(Node node) throws PathNotFoundException, ItemExistsException, NoSuchNodeTypeException, LockException, VersionException, ConstraintViolationException, RepositoryException {
            Node parNode = null;

            if (node.hasNode(BlogDataMigrationConstants.PAR)) {
                parNode = node.getNode(BlogDataMigrationConstants.PAR);
            } else {
                parNode = node.addNode(BlogDataMigrationConstants.PAR, BlogDataMigrationConstants.NODE_TYPE_UNSTRUCTURED);
            }

            parNode.setProperty(BlogDataMigrationConstants.PROPERTY_RESOURCE_TYPE, BlogDataMigrationConstants.PARSYS);

            return parNode;
        }


        private void createCaptionImageNode(Node parNode, String align, String src, String desc, String alt, String width, String height) throws AccessDeniedException, VersionException, LockException, ConstraintViolationException, PathNotFoundException, RepositoryException {
            int cnt = 0;
            if (parNode.hasNode(BlogDataMigrationConstants.CAPTIONEDIMAGE)) {
                NodeIterator nodes = parNode.getNodes("*" + BlogDataMigrationConstants.CAPTIONEDIMAGE + "*");
                while (nodes.hasNext()) {
                    cnt++;
                    nodes.next();
                }
            }
            String suffix = (cnt == 0) ? "" : ("_" + cnt);
            src = src.replaceAll("https://blog.asb.co.nz/wp-content", "/content/dam/ASB");
            logger.debug("Source >>> " + src);
            Node bannerNode = parNode.addNode(BlogDataMigrationConstants.CAPTIONEDIMAGE + suffix, BlogDataMigrationConstants.NODE_TYPE_UNSTRUCTURED);
            bannerNode.setProperty(BlogDataMigrationConstants.PROPERTY_RESOURCE_TYPE, BlogDataMigrationConstants.RESOURCE_CAPTIONEDIMAGE);
            bannerNode.setProperty(BlogDataMigrationConstants.PROPERTY_FILE_REFERENCE, src);
            bannerNode.setProperty(BlogDataMigrationConstants.PROPERTY_JCR_DESCRIPTION, desc);
            bannerNode.setProperty(BlogDataMigrationConstants.PROPERTY_ALIGNMENT, align);
            bannerNode.setProperty(BlogDataMigrationConstants.ALT, alt);
            bannerNode.setProperty(BlogDataMigrationConstants.PROPERTY_WIDTH, width);
            bannerNode.setProperty(BlogDataMigrationConstants.PROPERTY_HEIGHT, height);

        }

        private void createBannerImageNode(Node parNode, String bannerImageReference, String width, String height) throws AccessDeniedException, VersionException, LockException, ConstraintViolationException, PathNotFoundException, RepositoryException {
            if (parNode.hasNode(BlogDataMigrationConstants.BANNERIMAGE)) {
                parNode.getNode(BlogDataMigrationConstants.BANNERIMAGE).remove();
            }

            Node bannerNode = parNode.addNode(BlogDataMigrationConstants.BANNERIMAGE, BlogDataMigrationConstants.NODE_TYPE_UNSTRUCTURED);
            bannerNode.setProperty(BlogDataMigrationConstants.PROPERTY_RESOURCE_TYPE, BlogDataMigrationConstants.RESOURCE_BANNERIMAGE);
            bannerNode.setProperty(BlogDataMigrationConstants.PROPERTY_FILE_REFERENCE, bannerImageReference);

            if (width != null && height != null) {
                bannerNode.setProperty(BlogDataMigrationConstants.PROPERTY_WIDTH, width);
                bannerNode.setProperty(BlogDataMigrationConstants.PROPERTY_HEIGHT, height);
            }
        }

        private boolean createTextNode(Node parNode, String text, int count) throws AccessDeniedException, VersionException, LockException, ConstraintViolationException, PathNotFoundException, RepositoryException {
            boolean isNodeCreated = false;

            text = text.replaceAll("&nbsp;", "");

            if (text.trim().length() > 0) {
                String nodeName = null;
                if (count == 0) {
                    nodeName = BlogDataMigrationConstants.ENTRY;
                } else {
                    nodeName = "text_" + count;
                }

                if (parNode.hasNode(nodeName)) {
                    parNode.getNode(nodeName).remove();
                }

                Node textNode = parNode.addNode(nodeName, BlogDataMigrationConstants.NODE_TYPE_UNSTRUCTURED);
                textNode.setProperty(BlogDataMigrationConstants.PROPERTY_RESOURCE_TYPE, BlogDataMigrationConstants.TEXT);
                textNode.setProperty(BlogDataMigrationConstants.PROPERTY_TEXT_IS_RICH, "true");

                if (doesTextContainHTMLTags(text)) {
                    textNode.setProperty("text", text);
                } else {
                    textNode.setProperty("text", "<p>" + text + "</p>");
                }

                isNodeCreated = true;
            }

            return isNodeCreated;
        }

        private boolean createImageNode(Node parNode, String imageReference, String width, String height, int count) throws PathNotFoundException, ItemExistsException, NoSuchNodeTypeException, LockException, VersionException, ConstraintViolationException, RepositoryException {
            boolean isNodeCreated = false;

            String nodeName = null;
            if (count == 0) {
                nodeName = "image";
            } else {
                nodeName = "image_" + count;
            }

            Node imageNode = null;
            if (parNode.hasNode(nodeName)) {
                imageNode = parNode.getNode(nodeName);
            } else {
                imageNode = parNode.addNode(nodeName, BlogDataMigrationConstants.NODE_TYPE_UNSTRUCTURED);
            }

            imageNode.setProperty(BlogDataMigrationConstants.PROPERTY_RESOURCE_TYPE, BlogDataMigrationConstants.IMAGE);
            imageNode.setProperty(BlogDataMigrationConstants.PROPERTY_FILE_REFERENCE, imageReference);

            if (width != null && height != null) {
                imageNode.setProperty(BlogDataMigrationConstants.PROPERTY_WIDTH, width);
                imageNode.setProperty(BlogDataMigrationConstants.PROPERTY_HEIGHT, height);
            }

            isNodeCreated = true;
            return isNodeCreated;
        }

        private boolean createYouTubeNode(Node parNode, String youTubeReference, int count) throws PathNotFoundException, ItemExistsException, NoSuchNodeTypeException, LockException, VersionException, ConstraintViolationException, RepositoryException {
            boolean isNodeCreated = false;

            String nodeName = null;
            if (count == 0) {
                nodeName = "youtube";
            } else {
                nodeName = "youtube_" + count;
            }

            Node youtubeNode = null;
            if (parNode.hasNode(nodeName)) {
                youtubeNode = parNode.getNode(nodeName);
            } else {
                youtubeNode = parNode.addNode(nodeName, BlogDataMigrationConstants.NODE_TYPE_UNSTRUCTURED);
            }

            youtubeNode.setProperty(BlogDataMigrationConstants.PROPERTY_RESOURCE_TYPE, BlogDataMigrationConstants.YOUTUBE);
            youtubeNode.setProperty(BlogDataMigrationConstants.PROPERTY_REFERENCE, youTubeReference);

            isNodeCreated = true;
            return isNodeCreated;
        }

        private boolean createTwitterNode(Node parNode, String youTubeReference, int count) throws PathNotFoundException, ItemExistsException, NoSuchNodeTypeException, LockException, VersionException, ConstraintViolationException, RepositoryException {
            boolean isNodeCreated = false;

            String nodeName = null;
            if (count == 0) {
                nodeName = "twitter";
            } else {
                nodeName = "twitter_" + count;
            }

            Node twitterNode = null;
            if (parNode.hasNode(nodeName)) {
                twitterNode = parNode.getNode(nodeName);
            } else {
                twitterNode = parNode.addNode(nodeName, BlogDataMigrationConstants.NODE_TYPE_UNSTRUCTURED);
            }

            twitterNode.setProperty(BlogDataMigrationConstants.PROPERTY_RESOURCE_TYPE, BlogDataMigrationConstants.TWITTER);
            twitterNode.setProperty(BlogDataMigrationConstants.PROPERTY_REFERENCE, youTubeReference);

            isNodeCreated = true;
            return isNodeCreated;
        }

        private boolean doesTextContainHTMLTags(String text) {
            Document doc = Jsoup.parseBodyFragment(text);
            Element body = doc.body();
            return (body.children().size() > 0);
        }

        private class XmlItem {
            public String postParent;
            public String postId;
            public String title;
            public String description;
            public Date publishDate;
            public String link;
            public String nodeName;
            public List<String> categories = new LinkedList<String>();
            public List<String> tags = new LinkedList<String>();
            public String content;
            public Map<String, List<CommentItem>> commentsMap = new HashMap<String, List<CommentItem>>();
            public List<String> youtubeLinkList = new LinkedList<String>();
        }

        private class TagItem {
            public String tagSlug;
            public String tagName;
        }

        private class CommentItem {
            public String id;
            public String parentId;

            public String authorIP;
            public String authorUrl;
            public String commentTime;
            public String authorEmail;
            public String conmentDescription;
            public String userId;
        }
    }

}
