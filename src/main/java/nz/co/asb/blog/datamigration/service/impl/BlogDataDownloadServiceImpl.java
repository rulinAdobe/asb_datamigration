package nz.co.asb.blog.datamigration.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import nz.co.asb.blog.datamigration.constants.BlogDataMigrationConstants;
import nz.co.asb.blog.datamigration.service.BlogDataDownloadService;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.day.cq.dam.api.AssetManager;


@Component(immediate = true, label = "Download Blog Data Service", description = "Download blog data through url into CRX")
@Service
public class BlogDataDownloadServiceImpl implements BlogDataDownloadService {

	private Logger logger = LoggerFactory.getLogger(BlogDataMigrationServiceImpl.class);

	@Reference
	private ResourceResolverFactory resourceResolverFactory;
	
	public void downloadBlogData(InputStream xmlInputStream) {
		SAXParserFactory parserFactory = SAXParserFactory.newInstance();
		DefaultHandler handler = null;
		
		try
		{
			SAXParser parser = parserFactory.newSAXParser();
			handler = new ParseHandler(resourceResolverFactory);
			parser.parse(xmlInputStream, handler);
		}
		catch(Exception e)
		{
			logger.error(e.toString(), e);
		}
	}

	class ParseHandler extends DefaultHandler {	
		private Logger logger = LoggerFactory.getLogger(ParseHandler.class);
		
		private ResourceResolverFactory resourceResolverFactory = null;
		
		private Map<String, XmlItem> blogEntriesMap = new HashMap<String, XmlItem>();
		private Map<String, XmlItem> attachmentsMap = new HashMap<String, XmlItem>();
		
		private XmlItem currentBlogItem = null;
		private String currentValue = null;
		private Attributes currentAttributes = null;
		
		ParseHandler(ResourceResolverFactory resourceResolverFactory) {
			this.resourceResolverFactory = resourceResolverFactory;
		}
		
		public void startDocument() throws SAXException {
			
		}
		
		public void endDocument() throws SAXException  {
			Set<String> imageUrlSet = new HashSet<String>();
			ResourceResolver adminResourceResolver = null;
			
			// get image url from attachment url
			if(attachmentsMap != null && attachmentsMap.size() > 0) {
				for(XmlItem item : attachmentsMap.values()) {
					String imageUrl = item.attachmentUrl;
					
					if(imageUrl != null && imageUrl.length() > 0) {
						imageUrlSet.add(imageUrl);
					}
				}
			}
			
			// get image url for special instagram image
			String specialURL = "http://distilleryimage6.ak.instagram.com/171c65fa0ab411e38e2522000aeb0d71_6.jpg";
			imageUrlSet.add(specialURL);
			
			// get image url from blog page content
			if(blogEntriesMap != null && blogEntriesMap.size() > 0) {
				for(XmlItem item : blogEntriesMap.values()) {
					if (item.content != null && item.content.length() > 0) {
						Scanner contentScanner = new Scanner(item.content);
						
						try{
							while (contentScanner.hasNextLine()) {
								 String paragraph = contentScanner.nextLine().trim();
			                     if (paragraph.length() <= 0)
			                    	 continue;
			                     if (paragraph.matches(".*<img.*/>.*")) {
			                    	 Document doc = Jsoup.parse(paragraph);
			                    	 for(Element element : doc.select("img")) {
			                    		 String reference = element.attr("src");
			                    		 imageUrlSet.add(reference);
			                    	 } 
			                     }		                        
							 }	
						} finally {
							contentScanner.close();
						}	 	 
					}
				}
			}
			
			try {
				adminResourceResolver = resourceResolverFactory.getAdministrativeResourceResolver(null);
				AssetManager assetMgr = adminResourceResolver.adaptTo(AssetManager.class);
				
				for(String imageUrl : imageUrlSet) {
					URL url = null; 
					InputStream inputStream = null;
					String damPath = null;
					
					if(imageUrl.contains("upload")) {
						damPath = BlogDataMigrationConstants.PREFIX_DAM_IMAGE + imageUrl.substring(imageUrl.indexOf("upload"));
					} else {
						damPath = imageUrl.replace("http://", BlogDataMigrationConstants.PREFIX_DAM_IMAGE);
					}
					
					try {
						url = new URL(imageUrl);
						inputStream = url.openStream();
						assetMgr.createAsset(damPath, inputStream, "image", true);	
					} catch (MalformedURLException e) {
						logger.error(e.toString(), e);
					} catch (IOException e) {
						logger.error(e.toString(), e);
					} finally {
						if(null != inputStream) {
							try {
								inputStream.close();
							} catch (IOException e) {
								logger.error(e.toString(), e);
							}
						}
					}
				}
			} catch (LoginException e) {
				logger.error(e.toString(), e);
			} finally {
				if (adminResourceResolver != null) {
					adminResourceResolver.close();
				}
			}
		}
			
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {	
			if(BlogDataMigrationConstants.TAG_ITEM.equalsIgnoreCase(qName)) {
				currentBlogItem = new XmlItem();
			}
			
			currentAttributes = attributes;
		}
		
		public void endElement(String uri, String localName, String qName) throws SAXException {
			if(BlogDataMigrationConstants.TAG_ITEM.equalsIgnoreCase(qName)) {
				if(BlogDataMigrationConstants.VALUE_POST_PARENT_ZERO.equals(currentBlogItem.postParent)) {
					blogEntriesMap.put(currentBlogItem.postId, currentBlogItem);
				} else {
					attachmentsMap.put(currentBlogItem.postId, currentBlogItem);
				}
				
				currentBlogItem = null;
				
			} else if(BlogDataMigrationConstants.TAG_TITLE.equalsIgnoreCase(qName)) {
				if(currentBlogItem != null) {
					currentBlogItem.title = currentValue;
				}
			} else if(BlogDataMigrationConstants.TAG_DESCRIPTION.equalsIgnoreCase(qName)) {
				if(currentBlogItem != null) {
					currentBlogItem.description = currentValue;
				}
			} else if(BlogDataMigrationConstants.TAG_PUBLISH_DATE.equalsIgnoreCase(qName)) {
				if(currentBlogItem != null) {
					setPublishDate();
				}
			} else if(BlogDataMigrationConstants.TAG_LINK.equalsIgnoreCase(qName)) {
				if(currentBlogItem != null) {
					currentBlogItem.link = currentValue;
				}
			} else if(BlogDataMigrationConstants.TAG_CATEGORY.equalsIgnoreCase(qName)) {
				if(currentBlogItem != null && currentAttributes != null) {
					setCategoryOrTag();
				}
			} else if(BlogDataMigrationConstants.TAG_POST_ID.equalsIgnoreCase(qName)) {
				if(currentBlogItem != null) {
					currentBlogItem.postId = currentValue;
				}
			} else if(BlogDataMigrationConstants.TAG_POST_PARENT.equalsIgnoreCase(qName)) {
				if(currentBlogItem != null) {
					currentBlogItem.postParent = currentValue;
				}
			} else if(BlogDataMigrationConstants.TAG_CONTENT_ENCODED.equalsIgnoreCase(qName)) {
				if(currentBlogItem != null) {
					currentBlogItem.content = currentValue;
				}
			} else if (BlogDataMigrationConstants.TAG_ATTACHMENT_URL.equalsIgnoreCase(qName)) {
				if(currentBlogItem != null) {
					currentBlogItem.attachmentUrl = currentValue;
				}
			}
			
			currentAttributes = null;
			currentValue = null;
		}
		
		public void characters(char[] ch, int start, int length)
		{
			currentValue = new String(ch, start, length);
		}

		private void setCategoryOrTag() {
			for(int i=0; i<currentAttributes.getLength(); i++) {
				if(currentAttributes.getQName(i).equalsIgnoreCase("nicename")) {
					String value = currentAttributes.getValue(i);
					if(BlogDataMigrationConstants.CATEGORY_NEWS_STORIES.equalsIgnoreCase(value)
							|| BlogDataMigrationConstants.CATEGORY_TECH_INNOVATION.equalsIgnoreCase(value)
							|| BlogDataMigrationConstants.CATEGORY_YOUR_COMUNITY.equalsIgnoreCase(value)
							|| BlogDataMigrationConstants.CATEGORY_YOUR_MONEY.equalsIgnoreCase(value)) {

						if(value != null && value.length() > 0)
							currentBlogItem.categories.add(value);
					} else {
						if(currentValue != null && currentValue.length() > 0)
							currentBlogItem.tags.add(currentValue);
					}
				}
			}	
		}
		
		private void setPublishDate() {
			String[] array = currentValue.split("\\s");
			currentBlogItem.publishDate = BlogDataMigrationConstants.MONTHS.get(array[2].toLowerCase()) + "/" + array[1] + "/" + array[3];
		}
				
		private class XmlItem {
			public String postParent;
			public String postId;
			public String title;
			public String description;
			public String publishDate;
			public String link;
			public List<String> categories = new LinkedList<String>();
			public List<String> tags = new LinkedList<String>();
			public String content;
			public String attachmentUrl;
		}
	}
	


}
