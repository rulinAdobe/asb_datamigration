package nz.co.asb.blog.datamigration.service;

import java.io.InputStream;

public interface BlogDataDownloadService {
	void downloadBlogData(InputStream xmlInputStream);
}
